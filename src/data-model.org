#+TITLE: Data Model
#+AUTHOR: Thirumal Ravula
#+DATE: [2017-11-21 Tue]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document captures the data model for the services
  offered through API by a farmers' co-operative.  This
  application or the system is abstracted as the data model.
  The model captures: the entities that comprise the system;
  relations and constraints between these entities; and
  operations that entail a change in the state of the
  system.


* COMMENT Entities 
  1. Farm
  2. Crop
  3. Member
  4. FarmingMethods
  5. Inputs
  6. Produce
  7. Purchases
  8. Inventory
  9. Sales


* COMMENT Cardinality Relationships
The work on diagram is still in progress
#+caption: cardinality relationships
#+name: img-cr
#+BEGIN_EXPORT html
  <img src="https://docs.google.com/drawings/d/e/2PACX-1vQqx3Zi7TTD4ooCZL8_bZSK--fXTAFAu3-AFtdsQcKHUwa8nnYwEV-LuJqRwOzP7tsQnWtlZwBHPPF9/pub?w=960&amp;h=720">
#+END_EXPORT

[[https://docs.google.com/drawings/d/1I7r3ywE7wCDMcBscMDYylKeteAM17qXpsmWKUrtr2rc/edit][Edit Image]]


* Data Model for [[./requirements.org::Current%20Offering%20-%20Version%201.0.0][V1.0.0]] Requirements

** Entities
*** Product
**** Attributes
     1. Name
     2. Description

*** Variety
**** Attributes
     1. Name
     2. Price/Kg
     3. Description

*** Cooperative
**** Attributes
     1. Name
     2. Address
     3. Registered Number
     4. Website

*** Member
**** Attributes
     1. Name
     2. Crop Extent
     3. Estimated Output
     4. Actual Output
     5. Address

*** Order
**** Attributes
     1. Name
     2. Product
     3. Variety
     4. Number of Boxes
     5. Price

*** Status
**** Attributes
     1. Description

*** Address
**** Attributes
     1. Line 1
     2. Line 2
     3. Landmark
     4. pincode

*** DropLocation
    DropLocation is an address and is related to Order
**** Attributes

*** CostSplit
    This entity provides different cost splits.  CostSplit
    and Order are related.  An order has a particular cost
    split.
**** Attributes

*** User
    User and Order are related
**** Attributes
     1. Name
     2. GivenName
     3. FamilyName
     4. email-id

*** Role
    The roles are either =buyer= or =admin=.
**** Attributes
*** Session
    Contains information about the current session. 

** Notataion
   - =!= implies excactly one
   - =+= implies one or more
   - =?= zero or one
   - No symbol implies zero or more
** Cardinality Relationship Between the Entities
*** Product and Variety
    - varieties(Product) = Variety
    - product(Variety) = Product!
*** Product and Cooperative
    - products(Cooperative) = Product
    - cooperatives(Product) = Cooperative+
*** Member and Cooperative
    - members(Cooperative) = Member
    - cooperative(Member) = Cooperative!
*** Cooperative and Address
    - address(Cooperative) = Address!
    - cooperative(Address) = Cooperative?
*** Member and Address
    - address(Member) = Address!
    - cooperative(Address) = Member?

** Operations
   CREATE, READ, UPDATE and DELETE operations on all the
   entities are provided.  The access privileges of the role
   determine if an operation can be performed in a session.

*** getAllProducts
    - input :: session
    - output :: list of products
    - permissions :: allowed for both the roles.
    - effect :: None on the model

*** getVarietiesOfProduct
    - input :: session, product
    - output :: list of varieties
    - permissions :: allowed for both the roles.
    - effect :: None on the model

*** getProductsOfCooperative
    - input :: session, coop
    - output :: list of cooperatives
    - permissions :: allowed for both the roles.
    - effect :: None on the model

*** getCooperativesOfProduct
    - input :: session, product
    - output :: list of cooperatives
    - permissions :: allowed for both the roles.
    - effect :: None on the model

*** getAllCooperatives
    - input :: session
    - output :: list of cooperatives
    - permissions :: allowed for both the roles.
    - effect :: None on the model

*** getMembersOfCooperative
    - input :: session, coop
    - output :: list of members
    - permissions :: allowed for admin
    - effect :: None on the model
*** getCooperativeOfMember
    - input :: session, member
    - output :: coop
    - permissions :: allowed for admin
    - effect :: None on the model
*** getAddressOfCooperative
    - input :: session, member
    - output :: coop
    - permissions :: allowed for admin
    - effect :: None on the model

*** getAddressOfMember
    - input :: session, member
    - output :: address
    - permissions :: allowed for admin
    - effect :: None on the model


** REST API
