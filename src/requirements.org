#+TITLE: Requirements for API based services offered by farmers' cooperative
#+AUTHOR: Thirumal Ravula
#+DATE: [2017-11-21 Tue]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document captures the requirements for API based
  services offered by a farmers' co-operative.

* Master Requirements
  1. Ability to capture view the assets of a member like the
     farm details, farm equipment, livestock etc.

  2. Ability to capture, link and view the inputs to a crop
     in any farm owned by a member of a co-operative.  The
     inputs include seed, fertilizer, manure, pesticides,
     weedicide, etc.

  3. Ability to capture, link and view the farming methods
     engaged for a crop in any farm owned by a member of a
     co-operative.  The farming methods include mechanized
     tilling, manual weeding, inputs include seed,
     fertilizer, manure, pesticides, etc.

  4. Ability to capture, link and view the output of a crop
     in any farm owned by a member of a co-operative.

  5. Ability to capture, and view the purchases by the
     co-operative from a member and link to the purchase to
     a crop and a farm.

  6. Ability to still maintain the link of the produce
     purchased to the crop and farm even when the produce
     transforms due to value added processing.

  7. Ability to view the inventory and still be able to
     maintain the link of the item in the inventory to the
     crop and produce.

  8. Ability to list the inventory for sale. 

  9. Ability to list the sales and still maintain the link
     between the sale to the crop and the farm.

  10. Define Cooperative Maturity Model (CMM) levels, a pun
      on capability maturity model for a co-operative.  The
      CMM level determines the granularity of information
      captured, capability to serve this information through
      an API and the sustainability of the practices.

* Current Offering - Version 1.0.0
:PROPERTIES:
:CUSTOM_ID: v1.0.0
:END:
  This offering of the platform is limited by -
  1. The inventory is one single produce - mangoes, and its
     varieties like benishan, himayat etc.
  2. The payment option is BHIM.
  3. The delivery is made to certain predefined locations,
     from where the buyer picks up the order.
  4. The single block of purchase is either a 5kg and a 10kg
     box.

** Buyer features
   1. Provide the ability to view a variety of mangoes with
     their prices.
   2. Provide to view the members of the cooperative offering
      the produce for sale.
   3. Provide the ability to choose a variety and a quantity
      to purchase.
   4. Provide the ability to pay for the purchase.
   5. Provide the ability to choose a delivery location.
   6. Provide the ability to provide feedback on the purchase.
   7. Provide the ability to view the split of costs across
      the actors/function - farmer, platform, delivery
      service, payment service
** Admin features
   1. Provide the ability to perform CRUD operations on the
      inventory - inventory management.
   2. Provide the ability to perform CRUD operations on the
      information of the cooperative members - cooperative
      member management.
   3. Provide the ability to change the status of the
      orders - from order received, packed, out on delivery,
      delivered at a point and received by the consumer.
   4. Accounting.

